import React, { Component } from 'react';

class MovingEmails extends Component {
    constructor() {
        super();
        this.state = {
            percent: 0,
            disableStart: false,
        }
    }

    initiateAutoPercentRefresh = () => {
        this.percentInterval = window.setInterval(() => {
            this.props.getPercentMoved(this.props.shortid, res => {
                if(res.error === true){
                    this.setState({percentError: `We are having some issues showing the percentage. Don't worry, your emails are being moved. We will send you an email when we finish moving them.`})
                }
                
                if(res.data.percent !== undefined) {
                    this.setState({percent: res.data.percent});
                }
                if(this.state.percent === 100) {
                    this.props.completed();
                    this.destroyAutoPercentRefresh();
                }
            });
        }, 2000);
    }

    destroyAutoPercentRefresh = () => {
        window.clearInterval(this.percentInterval);
    }

    componentWillUnmount() {
        this.destroyAutoPercentRefresh();
    }

    cancelME = () => {
        this.setState({cancelingStatus: "Canceling..."});
        this.props.cancelPPP((res) => {
            if(res.data.error === false && res.data.payment_canceled === true) {
                this.setState({cancelingStatus: "Payment refunded. Mailboxes configuration removed from our servers.", startOver: true});
            } else {
                this.setState({cancelingStatus: `${res.data.message} Contact us at: help@mydomain.com`});
            }
        });
    }

    submitPayload = () => {
        this.props.submitPayload();
        this.setState({
            disableStart: true
        })
        this.initiateAutoPercentRefresh();
    }   

    render() {
        const { move_status = 0 } = this.props.data;

        return (
            <div className="MovingEmails">
                {move_status === -1 ? (
                    <div className="MEView errorScreen">
                        <div className="status error">Error to initiate the transfer of your emails.</div>
                        {typeof this.state.cancelingStatus !== "undefined" ? <div className="cancelingStatus">{this.state.cancelingStatus}</div> : null}
                        {this.state.startOver !== true 
                            ? <button className="button" onClick={this.cancelME}>Cancel and Refund</button> 
                            : <button className="button" onClick={ (e) => this.props.startOver() }>Start Over?</button>} 
                    </div>
                ) : (
                    <div>
                        <div className="MEView">
                            <div className="sides percentView border-effect">
                                <div className="view">
                                    <div className="percentBorder">
                                        <p>{this.state.percent}%</p>
                                        <span>completed</span>
                                    </div>
                                </div>
                            </div>
                            <div className="sides checkList">
                                <div className="view">
                                    <h3>Your ID:</h3>
                                    <input type="text" value={this.props.shortid} readOnly={true} />
                                    <p>Use this ID to keep track of the status of your email transfer at: mydomain.com/status</p>
                                </div>
                            </div>
                            
                        </div>
                        <div className="description">
                            {move_status === 1 ? (
                                    <p>{typeof this.state.percentError !== "undefined" ? this.state.percentError : 'Moving your emails now!'}</p>
                                ) : (
                                    <div>
                                        {move_status === 99 ? (
                                            <p>All of your emails were moved successfully!</p>
                                        ) : <p>Ready to move your emails?</p>}
                                    </div>
                                )}
                        </div>
                        {move_status === 1 ? (
                            <button className="button centerThis">Cancel</button>
                        ) : (
                            <div>
                                {move_status === 99 ? (
                                    <button className="button centerThis" onClick={ () => this.props.startOver() }>Start New Transfer</button>
                                ) : (
                                    <div className="ButtonPack">
                                        <button className="button" onClick={ (e) => this.props.previousStep() }>Back</button>
                                        <button className="button" disabled={this.state.disableStart} onClick={this.submitPayload}>Start</button>
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                )}
            </div>
        );
    }
}

export default MovingEmails;