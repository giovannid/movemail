import React, { Component } from 'react';
import MailboxForm from '../components/MailboxForm';

class CollectingData extends Component {
    constructor(){
        super();
        this.state = {
            nextDisabled: false
        }
    }

    handleNext = () => {
        this.setState({nextDisabled: true});
        this.props.checkConnection( (finished) => {
            if( this.props.data.mailbox_one.connectionCheck === true
                && this.props.data.mailbox_two.connectionCheck === true ) {
                this.delayNext = setTimeout( () => {
                    this.props.nextStep();
                }, 1000);
            } else {
                this.setState({nextDisabled: false});
            }
        });
    }

    componentWillUnmount() {
        window.clearTimeout(this.delayNext);
    }

    render() {
        return (
             <div className="CollectingData">
                <MailboxForm data={this.props.data.mailbox_one} saveData={this.props.saveData} title={"Old Mailbox"} id="mailbox_one" />
                <MailboxForm data={this.props.data.mailbox_two} saveData={this.props.saveData} title={"New Mailbox"} id="mailbox_two" />
                <div className="description">
                    <p>Emails from your Old Mailbox will be moved to the New Mailbox.</p>
                </div>
                <button disabled={this.state.nextDisabled} onClick={this.handleNext} className="button">Next</button>
            </div>
        );
    }
}

export default CollectingData;