import React, { Component } from 'react';
import CheckBox from '../components/CheckBox';

class SelectFolders extends Component {
    constructor(props){
        super(props);
        this.state = {}
    }

    handleFolderSelected = (label) => {
        const fSelected = this.props.data.foldersSelected || [];

        let index = fSelected.indexOf(label);
        if( index === -1 ) {
            fSelected.push(label);
        } else {
            fSelected.splice(index, 1);
        }
        if( fSelected.length > 0 && this.state.select_error === true ) {
            this.setState({ select_error: false });
        }
        if( this.state.zero_emails === true ) {
            this.setState({ zero_emails: false });
        }
        this.props.saveData({
            foldersSelected: fSelected
        }, 'selecting_folders');
    }

    componentDidMount() {
        this.props.getFolders();
    }

    renderFolders = ( fStruct, fSelected ) => {
        let outer = this;

        function renderChildren(data) {
            if(data === null || data === undefined || Object.keys(data).length === 0) return null;
            let _this = outer;
            let node = Object.keys(data).map( (value, index) => {
                let output = [];
                output.push(<CheckBox path={data[value].path} 
                                label={value} 
                                startChecked={ fSelected.length > 0 && fSelected.indexOf(data[value].path) !== -1 ? true : false } 
                                key={data[value].path} 
                                handleCheckboxChange={_this.handleFolderSelected} />);

                if ( Object.keys(data[value].children).length > 0 ) {
                    output.push(renderChildren(data[value].children));
                }
                return output;
            });
            return node;
        }
        
        return renderChildren(fStruct) === null ? null : (
            <div className="FolderStruct">
                {renderChildren(fStruct)}
            </div>
        );
    }

    renderLoading = () => {
        return (
            <div className="LoadingFolders">
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="load_block"></div>
                <div className="status trying"><span>Retrieving folders</span></div>
            </div>
        );
    }
    
    handleNext = () => {
        const fSelected = this.props.data.foldersSelected || [];
        if( fSelected.length > 0 ) {
            if( this.countEmails() === 0 ) {
                this.setState({
                    zero_emails: true
                });
            } else {
                this.props.nextStep();
            }
        } else {
            this.setState({
                select_error: true
            });
        }
    }

    getQntFromFolderStruct(node, value ) {
        let v;
        for(const item in node) {
            if( node.hasOwnProperty(item) ) {
                if(node[item].path === value) {
                    v = node[item].qnt;
                } else {
                    if( v === undefined ) {
                        if( Object.keys(node[item].children).length > 0 ) {
                            v = this.getQntFromFolderStruct(node[item].children, value);
                        }
                    }
                }
            }
        }
        return v;
    }

    countEmails = () => {
        const fStruct   = this.props.data.folderStruct    || {};
        const fSelected = this.props.data.foldersSelected || [];

        let total = 0;
        if( Object.keys(fStruct).length > 0 && fSelected.length > 0 ) {
            for(let i = 0; i <  fSelected.length; i++) {
                let number = this.getQntFromFolderStruct( this.props.data.folderStruct, this.props.data.foldersSelected[i] );
                total += (number !== undefined && Number.isNaN(number) !== true) ? number : 0;
            }
        }
        return total;
    }

    render() {
        const { retrievingFolders = true, foldersSelected = [], folderStruct = {} } = this.props.data;

        let error_retrieving = retrievingFolders === false && Object.keys(folderStruct).length === 0 ? true : false;

        let folders = this.renderFolders(folderStruct, foldersSelected);

        if (folders === null || retrievingFolders === true) {
            folders = this.renderLoading();
        }

        return (
            <div className="SelectFolders">
                <h2>Folders on your Old Mailbox</h2>
                
                <div className="SelectFixed">

                    { error_retrieving !== true ?  

                        <div>
                            <div className="SelectSection">
                                {folders}
                            </div>
                            
                            { retrievingFolders !== true ? 
                                <div>
                                    { this.state.zero_emails === true 
                                    ? <div className="status error">There are no emails inside this folder!</div>
                                    : this.state.select_error === true && foldersSelected.length === 0
                                    ? <div className="status error">Select at least one folder!</div>
                                    : <div className="status info">{foldersSelected.length} folder{foldersSelected.length > 1 ? 's' : ''} selected - {this.countEmails()} emails total</div> }
                                </div>
                            : null }    
                            
                        </div>
                    
                    : <div className="status error">Error retrieving folders!</div> }

                </div>

                <div className="description">
                    <p>Select additional folders you want to move to your New Mailbox.</p>
                </div>
                <div className="ButtonPack">
                    <button className="button" disabled={retrievingFolders} onClick={ (e) => this.props.previousStep() }>Back</button>
                    <button className="button" disabled={retrievingFolders}  onClick={this.handleNext}>Next</button>
                </div>
            </div>
        );
    }
}

export default SelectFolders;