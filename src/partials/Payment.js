/* eslint-disable */
import React, { Component } from 'react';
import PayPalButton from '../components/PayPalButton';
import convertSecs from 'convert-seconds';

class Payment extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.getEmailCount();
    }
    
    renderLoading = (msg) => {
        return (
            <div className="PaymentLoading">
                <div className="status trying"><span>{typeof this.props.data.loadingMsg !== "undefined" && this.props.data.loadingMsg !== null ? this.props.data.loadingMsg : msg}</span></div>
            </div>
        );
    }

    renderETA = (total_emails) => {
        let eps = 13;
        let secs = Math.round(total_emails / eps);
        
        let eta = convertSecs(secs);
        if( eta.hours === 0 && eta.minutes === 0 ) {
            return `${eta.seconds}s`;
        } else if( eta.hours === 0 ) {
            return `${eta.minutes}m${eta.seconds}s`;
        } else {
            return `${eta.hours}h${eta.minutes}m`;
        }
    }
 
    renderPayPalButton = (total_price, total_emails) => {
        return <PayPalButton shortid={this.props.shortid} createPPP={this.props.createPPP} executePPP={this.props.executePPP} amount={total_price} total_emails={total_emails} saveData={this.props.saveData} />;
    }

    renderPaymentStatus = (paymentStatus) => {
        // 1 > Done
        // -1 > Canceled
        // 2 > Error Refunding paymentng
        // 3 > Error authorizing
        switch (paymentStatus) {
            case 1:
                return (
                    <div className="PaymentDone">
                        Payment Done
                    </div> 
                );
            case -1:
                return (
                    <div className="PaymentCanceled">
                        Payment Canceled
                    </div> 
                );
            case 2: 
                return (
                    <div className="PaymentCanceled">
                        We couldn't cancel the payment. Contact us via the email: help@mydomain.com
                    </div> 
                );
            case 3: 
                return (
                    <div className="PaymentCanceled">
                        Problem authorizing the payment. Try again in a few minutes.
                    </div> 
                );
            default:
                return null;
        }
        
    }

    cancelAndRefund = () => {
        this.props.saveData({ processingPayment: true, loadingMsg: "Refunding payment" }, 'payment');
        this.props.cancelPPP((res) => {
            if( res.data.error === true ) {
                this.props.saveData({processingPayment: false, loadingMsg: null, paymentStatus: 2 }, 'payment');
            } else if( res.data.error === false && res.data.payment_canceled === true ) {
                this.props.saveData({processingPayment: false, loadingMsg: null, paymentStatus: -1 }, 'payment');
            }
        });
    }

    handlePrevious = () => {
        const { paymentStatus = 0 } = this.props.data;
        if( paymentStatus === -1 ) {
            this.props.saveData({ paymentStatus: 0 }, 'payment');
        }
        this.props.previousStep();
    }

    render() {
        const { retrievingEmailCount = true, total_price, total_emails, ppe, paymentStatus = 0, processingPayment = false } = this.props.data;
        return (
            <div className="Payment">
                <h2>Confirm the details</h2>
                <div className="PaymentStats">
                    { retrievingEmailCount === true 
                    ?   this.renderLoading('Loading payment details')
                    :   total_price === 0 && total_emails === 0 
                    
                        ?   <div className="noEmails"> <div className="status error">Error! Try again later.</div> </div>
                        :   <div>
                                <div className="PStatsBlocks">
                                    <div className="PStatsItem">
                                        <span>Total of Emails</span>
                                        <h3>{total_emails.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</h3>
                                    </div>
                                    <div className="PStatsSeparator" />
                                    <div className="PStatsItem">
                                        <span>ETA to Complete</span> 
                                        <h3>{this.renderETA(total_emails)}</h3>
                                    </div>
                                    <div className="PStatsSeparator" />
                                    
                                    <div className="PStatsItem">
                                        <span>Price per Email</span>
                                        <h3>${ppe}</h3>
                                    </div>
                                    <div className="PStatsSeparator" />
                                    <div className="PStatsItem">
                                        <span>Total Amount</span>
                                        <h3>${total_price}</h3>
                                    </div>
                                </div>

                                <div className="PaymentArea">
                                    <div>
                                    { paymentStatus === 0
                                        ? <div className="PayPalButtonStyle">
                                                <p>Pay with PayPal</p>
                                                {this.renderPayPalButton(total_price, total_emails)}
                                            </div> 
                                        : this.renderPaymentStatus(paymentStatus) }
                                    { processingPayment === true ? this.renderLoading('Processing Payment') : null}
                                    </div>
                                </div>
                            </div> }
                </div>
                <div className="description">
                    <p>We will only take your money after we transfer all of your emails.</p>
                </div>
                { total_emails === 0 ? <button className="button centerThis" onClick={ (e) => this.props.previousStep() }>Back</button> : 
                    <div className="ButtonPack">
                        {paymentStatus === 1 
                        ? <button className="button cancel_refund" onClick={ (e) => this.cancelAndRefund() }>Cancel and Refund</button>
                        : <button className="button" onClick={ (e) => this.handlePrevious() }>Back</button> }
                        <button className="button" onClick={ (e) => this.props.nextStep() } disabled={ paymentStatus === 1 ? false : true }>Next</button>
                    </div>
                }
            </div>
        );
    }
}

export default Payment;