import React, { Component } from 'react';

class CheckBox extends Component {
    constructor(props){
        super(props);
        this.state = {
            isChecked: false
        }
    }

    componentWillMount() {
        if(this.props.startChecked === true) {
            this.setState({isChecked: true});
        }
    }

    toggleCheckboxChange = () => {
        const { handleCheckboxChange, path } = this.props;

        this.setState(  (prevState) => ({
            isChecked: !prevState.isChecked
        }));

        handleCheckboxChange(path);
    }

    render() {
        const { label, path } = this.props;
        const { isChecked }   = this.state;

        return (
            <div className="checkbox">
                <label>
                    <input  type="checkbox"
                            value={path}
                            checked={isChecked}
                            onChange={this.toggleCheckboxChange}
                    />
                    <span>{label}</span>
                </label>
            </div>
        );
    }
}

export default CheckBox;