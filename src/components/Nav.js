import React, { Component } from 'react';

class Nav extends Component {

    render() {
        let ulStyle = {
            marginLeft: -( (this.props.selected - 1) !== 0 ? (this.props.selected - 1) * 350 : 0) + 'px'
        }
        return (
            <div className="Navigation">
                <ul style={ulStyle}>
                    {this.props.data.map((item, index) => 
                        <li className={'el' + index + ' ' +  (index + 1 === this.props.selected ? 'selected' : '') + (index + 1 === 1 ? ' elzero' : '')} key={index}>{item}</li>    
                    )}
                </ul>
                <div className="navigation_effect"></div>
            </div>
        );
    }
}

export default Nav;
