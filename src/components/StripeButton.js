import React, { Component } from 'react';

class StripeButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
           loadError: false,
           loadSuccess: false
        }

    }

    componentWillMount() {
        if( window.StripeCheckout === undefined ) {
            const script = document.createElement("script");
            let self = this;
            script.src = "https://checkout.stripe.com/checkout.js";
            script.async = true;
            script.type = "text/javascript";
            script.onerror = function() {
                console.log('error!');
                self.setState({ loadError: true });
            }
            script.onload = function() {
                console.log('loaded');
                self.setState({ loadSuccess: true,});
                self.configStripe();
            }
            document.body.appendChild(script);
        } else {
            this.configStripe();
        }
        
    }

    configStripe = () => {
        this.setState({
            handler: window.StripeCheckout.configure({
                key: 'BLABLABLA',
                image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                locale: 'auto',
                token: function(token) {
                    console.log(token)
                }
            })
        })
    }

    componentWillUnmount() {
        // Clean stripe dom elements
        let stripeDOM = document.querySelectorAll('.stripe_checkout_app');
        stripeDOM.forEach((item) => {
            if( item.parentNode ) {
                item.parentNode.removeChild(item);
            }
        });
    }

    handleClick = (e) => {
        let numberEmails = this.props.numberEmails.toString().split(".");
        numberEmails[0] = numberEmails[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        numberEmails = numberEmails.join(".");
        
        this.state.handler.open({
            name: 'Move Emails',
            description: `Cost to move ${numberEmails} emails.`,
            amount: this.props.amount * 100,
            allowRememberMe: false
        });
        e.preventDefault();
    }

    render() {
        return (
            <div>
               <button onClick={this.handleClick}>Pay</button>
            </div>
        );
    }
}

export default StripeButton;