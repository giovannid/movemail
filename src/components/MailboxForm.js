import React, { Component } from 'react';

class MailboxForm extends Component {
    constructor(props){
        super(props);
        this.state = {
            hostname: '',
            email: '',
            password: '',
            port: '',
            ssl: false
        }
    }

    componentDidMount() {
        if( Object.keys(this.props.data).length > 0 ) {
            this.setState(this.props.data);
        }
    }
   
    handleChange = (e) => {
        let name = e.target.dataset.name;
        let value = e.target.value;
        this.setState( prevState => ({  
            [name]: value
        }), this.saveData);
    }

    handleCheckBox = (e) => {
        this.setState((prevState) => ({
            ssl: !prevState.ssl
        }), this.saveData);
    }

    saveData = (e) => {
        this.props.saveData(this.state, this.props.id);
    }

    showStatus = () => {
        let tryingConnection = this.props.data.tryingConnection || false;
        let connectionCheck  = this.props.data.connectionCheck;
        
        if( tryingConnection === true ) {
            return <div className="status trying"><span>Trying connection</span></div>
        } 
        
        if( connectionCheck === true ) {
            return <div className="status success">Successfully connected</div>
        } else if ( connectionCheck === false ) {
            return <div className="status error">Connection failed</div>
        }
    }

    render() {
        

        return (
            <div className="MailboxForm">
                <div className="form">
                <h2>{this.props.title}</h2>
                <input type="text"     data-name="hostname" value={this.state.hostname} onChange={this.handleChange} placeholder={"Hostname"} />
                <input type="text"     data-name="email"    value={this.state.email}    onChange={this.handleChange} placeholder={"Email"} />
                <input type="password" data-name="password" value={this.state.password} onChange={this.handleChange} placeholder={"Password"} />
                <div className="port_and_ssl">
                    <input type="text" data-name="port"     value={this.state.port}     onChange={this.handleChange} placeholder={"Port"} maxLength="10" />
                    <div className="ssl_toggle">
                        <label htmlFor={'ssl_toggle_' + this.props.id}>
                            <input type="checkbox" data-name="ssl"      checked={this.state.ssl}    onChange={this.handleCheckBox} id={'ssl_toggle_' + this.props.id} />
                            <span>SSL</span>
                        </label>
                    </div>
                </div>
                {this.showStatus()}
                </div>
            </div>
        );
    }
}

export default MailboxForm;