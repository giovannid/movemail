export default {
    folders: {"folder_struct":{"INBOX":{"path":"INBOX","qnt":15,"errorRetrievingQnt":false,"children":{"Archive":{"path":"INBOX.Archive","qnt":10,"errorRetrievingQnt":false,"children":{}},"spam":{"path":"INBOX.spam","qnt":5,"errorRetrievingQnt":false,"children":{}},"Junk":{"path":"INBOX.Junk","qnt":0,"errorRetrievingQnt":false,"children":{}},"Sent":{"path":"INBOX.Sent","qnt":6,"errorRetrievingQnt":false,"children":{}},"Drafts":{"path":"INBOX.Drafts","qnt":5,"errorRetrievingQnt":false,"children":{}},"Trash":{"path":"INBOX.Trash","qnt":5,"errorRetrievingQnt":false,"children":{}}}}}},
    count_mails: function(selectedFolders) {
        let total = 0;
        selectedFolders.forEach(item => {
            if(this.folders['folder_struct'][item]){
                total += this.folders['folder_struct'][item].qnt;
            } else {
                let child = item.slice(6);
                total += this.folders['folder_struct']['INBOX'].children[child].qnt
            }
        });
        return total;
    },
    returnPromise: function(out) {
        return new Promise(resolve => {
            setTimeout(_ => {
                resolve({ data: out });
            }, 500);
        });
    },
    post: function(path, payload, headers) {
        console.log(`RECEIVING: ${path}`, payload, headers);
        switch(path) {
            case '/token':
                return this.returnPromise({ error: false, token: 'AAA' });
            case '/v1/check-connection':
                if( (payload.email === "one@giovannid.com" || payload.email === "two@giovannid.com")
                    && payload.hostname === "mail.giovannid.com" 
                    && payload.password === "123" 
                    && (payload.port === 993 || payload.port === 143)
                    && (payload.ssl === true || payload.ssl === false)) {
                    return this.returnPromise({error: false, connection: true});
                }
                return this.returnPromise({error: true, message: "Missing parameters." });
            case '/v1/folder-struct':
                return this.returnPromise(this.folders);
            case '/v1/count-emails':
                return this.returnPromise({error: false, total_emails: this.count_mails(payload.selectedFolders), total_price: (this.count_mails(payload.selectedFolders) * 0.00095).toFixed(2), ppe: 0.00095});
            case '/v1/cancel-payment':
                return this.returnPromise({error: false, payment_canceled: true});
            case '/v1/start-move':
                return this.returnPromise({error: false, moving_emails: true});
            case '/v1/get-percent/rJFkPX0_l':
                return this.returnPromise({error: false, percent: Math.random() > 0.7 ? 100 : 50 });
            default:
                return this.returnPromise({ error: true, message: "Endpoint not found!" });
        }
    },
    get: function(path, payload, headers) {
        return this.post(path, payload, headers);
    }
}