/* eslint-disable */
// import axios from 'axios';
import DummyServer from './DummyServer';

// const apiURL = "http://127.0.0.1:8080";
// const endpoint = axios.create({
//     baseURL: apiURL,
//     timeout: 10000
// }); 

const endpoint = DummyServer;

export function retrieveToken( callback ) {
    endpoint.post('/token').then((res) => {
        callback(res);
    }).catch((err) => {
        callback({ data: { error: true }});
    });
}

function prepareMailboxReq(path, token, mailbox = {}, additionalData = {}) {
    let payload = Object.assign(mailbox, additionalData);
    return endpoint.post(path, payload, {
        headers: {'x-access-token': token}
    });
}

export function checkMailboxConnection(token, mailbox, callback ) {
    if( typeof mailbox !== 'object' && mailbox === null ) callback(false);
    prepareMailboxReq('/v1/check-connection', token, mailbox)
    .then((res) => {
        if(res.data.connection === true) {
            callback(true);
        } else {
            callback(false);
        }
    }).catch((err) => {
        if(err) callback(false);
    });
}

export function getMailboxFolders(token, mailbox, callback ) {
    if( typeof mailbox !== 'object' && mailbox === null ) callback(null);
    prepareMailboxReq('/v1/folder-struct', token, mailbox)
    .then((res) => {
        if(typeof res.data.folder_struct !== "undefined" && Object.keys(res.data.folder_struct).length > 0) {
            callback(res.data.folder_struct);
        } else {
            callback({});
        }
    }).catch((err) => {
        console.log(err);
        if(err) callback({});
    });
}

export function getMailboxEmailCount(token, mailbox, selectedFolders, callback ) {
    if( typeof mailbox !== 'object' && mailbox === null ) callback(false);
    prepareMailboxReq('/v1/count-emails', token, mailbox, {selectedFolders: selectedFolders})
    .then((res) => {
        if(Object.keys(res.data).length > 0) {
            callback(res.data);
        } else {
            callback({ data: { error: true }});
        }
    }).catch((err) => {
        console.log(err);
        if(err) callback({ data: { error: true }});
    });
}

export function createPayPalPayment(token, callback) {
    endpoint.post('/v1/create-payment', null, {
        headers: {'x-access-token': token}
    }).then((res) => {
        callback(res);
    }).catch((err) => {
        callback({ data: { error: true }});
    });
}

export function executePayPalPayment(token, data, callback) {
    endpoint.post('/v1/execute-payment', data, {
        headers: {'x-access-token': token}
    }).then((res) => {
        callback(res);
    }).catch((err) => {
        callback({ data: { error: true }});
    });
}

export function cancelPayPalPayment(token, callback) {
    endpoint.post('/v1/cancel-payment', null, {
        headers: {'x-access-token': token}
    }).then((res) => {
        callback(res);
    }).catch((err) => {
        callback({ data: { error: true }});
    });
}

export function submitPayLoad(token, data,  callback ) {
    if( typeof data !== 'object' || data === null ) callback({ data: { error: true } });
    endpoint.post('/v1/start-move', data, {
        headers: {'x-access-token': token}
    }).then((res) => {
        callback(res);
    }).catch((err) => {
        if(err) callback({ data: { error: true } });
    });
}

export function getPercentMoved( token, shortid, callback ) {
    if( typeof token !== 'object' || token === null ) callback({ data: { error: true } });
    endpoint.get(`/v1/get-percent/${shortid}`, {
        headers: {'x-access-token': token}
    }).then((res) => {
       callback(res);
    }).catch((err) => {
        if(err) callback({error: true});
    });
}