/* eslint-disable */
import React, { Component }     from 'react';
import { checkMailboxConnection, 
         getMailboxFolders,
         getMailboxEmailCount,
         submitPayLoad,
         getPercentMoved,
         createPayPalPayment,
         executePayPalPayment,
         cancelPayPalPayment,
         retrieveToken }        from './Api';
import CollectingData           from './partials/CollectingData';
import SelectFolders            from './partials/SelectFolders_new';
import MovingEmails             from './partials/MovingEmails';
import Payment                  from './partials/Payment';
import Nav                      from './components/Nav';
import async from 'async';

class App extends Component {
    constructor(){
        super();
        this.state = { 
            tabNames: ['Collecting Data', 'Select Folders', 'Payment', 'Moving Emails'], 
            mailbox_one: {
                hostname: 'mail.giovannid.com',
                email: 'one@giovannid.com',
                password: '123',
                port: 143,
                ssl: false,
                connectionCheck: null,
                tryingConnection: false
            },
            mailbox_two: {
                hostname: 'mail.giovannid.com',
                email: 'two@giovannid.com',
                password: '123',
                port: 993,
                ssl: true,
                connectionCheck: null,
                tryingConnection: false
            },
            tab: 1,
            payment: {
                paymentStatus: 1
            },
            selecting_folders: {
                foldersSelected: ['INBOX']
            },
            token: "BLA",
            shortid: 'rJFkPX0_l'
        }
    }

    saveData = (data, parent = null, callback) => {
        if(parent !== null ) {
            this.setState(prevState =>  {
                let parentNode = Object.assign({}, prevState[parent], data );
                return { [parent]: parentNode }
            }, callback);
        } else {
            this.setState(prevState =>  Object.assign({}, prevState, data), callback);
        }
    }

    nextStep = () => {
        this.setState( prevState => ({
            tab: prevState.tab + 1
        }));
    }

    previousStep = () => {
       this.setState( prevState => ({
            tab: prevState.tab - 1
        }))
    }

    submitPayloadFromApp = ( callback ) => {
        submitPayLoad(this.state.token, this.state, (res) => {
            if(res.data.error === false && res.data.moving_emails === true) {
                this.saveData({ move_status: 1}, 'moving_emails');
            } else {
                this.saveData({ move_status: -1}, 'moving_emails');
            }
        });
    }

    moveCompleted = () => {
        this.saveData({ move_status: 99}, 'moving_emails');
    }

    getPercent = (shortid, callback) => {
        getPercentMoved(this.state.token, shortid, res => callback(res));
    } 

    getToken = (callback) => {
        retrieveToken((res) => {
            if(res.data.error === false) {
                this.saveData({token: res.data.token}, null, () => {
                    callback(true);
                });
            } else {
                callback(false);
            }
        })
    }

    checkConnection = ( finished ) => {
        this.getToken((status) => {
            if(status === true) {
                this.saveData({ tryingConnection: true }, 'mailbox_one');
                this.saveData({ tryingConnection: true }, 'mailbox_two');
                async.each(['mailbox_one', 'mailbox_two'], (box, callback) => {
                    checkMailboxConnection(this.state.token, this.state[box], (res) => {
                        this.saveData({ connectionCheck: res, tryingConnection: false }, box);
                        callback(null);
                    });
                }, (err) => {
                    finished(true);
                });
            } else {
                this.saveData({errorRetrievingToken: true});
            }
        });
    }

    getFolders = () => {
        this.saveData({ retrievingFolders: true }, 'selecting_folders');
        getMailboxFolders(this.state.token, this.state.mailbox_one, (res) =>  {
            this.saveData({ 
                retrievingFolders: false,
                folderStruct: res
            }, 'selecting_folders');
        });
    }

    getEmailCount = (callback) => {
        this.saveData({ retrievingEmailCount: true }, 'payment');
        const foldersSelected = this.state.selecting_folders && this.state.selecting_folders.foldersSelected;
        getMailboxEmailCount(this.state.token, this.state.mailbox_one, foldersSelected, (res) => {
            if( res.error === false ) {
                this.saveData({
                    retrievingEmailCount: false,
                    total_emails: res.total_emails,
                    total_price: res.total_price,
                    ppe: res.ppe
                }, 'payment');
            } else if( res.error === true ) {
                this.saveData({
                    retrievingEmailCount: false,
                    total_emails: 0,
                    total_price: 0,
                    ppe: 0
                }, 'payment');
            }
        });
    }

    createPayment = (callback) => {
        createPayPalPayment(this.state.token, (res) => {
            callback(res);
        });
    }

    executePayment = (data, callback) => {
        executePayPalPayment(this.state.token, data, (res) => {
            callback(res);
        });
    }

    cancelPayment = (callback) => {
        cancelPayPalPayment(this.state.token, (res) => {
            callback(res);
        })
    }

    startOver = () => {
        this.setState({
            mailbox_one: {},
            mailbox_two: {},
            tab: 1,
            payment: {},
            selecting_folders: {},
            token: null,
            shortid: null
        });
    }

    showView = () => {
        let collecting_data = {
            mailbox_one: this.state.mailbox_one || {},
            mailbox_two: this.state.mailbox_two || {}
        };

        let selecting_folders = this.state.selecting_folders || {};
        let payment = this.state.payment || {};
        let moving_emails = this.state.moving_emails || {};

        switch (this.state.tab) {
            case 1:
                return <CollectingData  data={collecting_data}
                                        nextStep={this.nextStep}
                                        checkConnection={this.checkConnection}
                                        saveData={this.saveData} />
            case 2:
                return <SelectFolders   data={selecting_folders}
                                        getFolders={this.getFolders}
                                        nextStep={this.nextStep}
                                        previousStep={this.previousStep}
                                        saveData={this.saveData}
                                        teste={this.createPayment} />
            case 3:
                return <Payment         data={payment}
                                        getEmailCount={this.getEmailCount}
                                        nextStep={this.nextStep}
                                        previousStep={this.previousStep}
                                        saveData={this.saveData}
                                        shortid={this.state.shortid}
                                        createPPP={this.createPayment}
                                        executePPP={this.executePayment}
                                        cancelPPP={this.cancelPayment} />
            case 4:
                return <MovingEmails    data={moving_emails}
                                        submitPayload={this.submitPayloadFromApp}
                                        shortid={this.state.shortid}
                                        getPercentMoved={this.getPercent}
                                        previousStep={this.previousStep}
                                        completed={this.moveCompleted}
                                        cancelPPP={this.cancelPayment}
                                        startOver={this.startOver} />
            default:
                return null;
        }
    }

    getData = () => {
        console.table(this.state);
    }

    render() {
        let { errorRetrievingToken = null } = this.state;

        return (
            <div className="App">
                <Nav data={this.state.tabNames} selected={this.state.tab} />
                { errorRetrievingToken === true ? (
                    <div className="errorRetrievingToken">
                        <i className="fa fa-exclamation-triangle"></i>
                        <p>We are experiencing some issues with our services. Please try later.</p>
                    </div>
                ) : ( 
                    <div className="View">
                        {this.showView()}
                    </div>
                )}
                <button onClick={this.getData}>getdata</button>
                <button onClick={ () => this.setState({tab: this.state.tab + 1})}>plus</button>
                <button onClick={ () => this.setState({tab: this.state.tab - 1})}>minus</button>
            </div>
        );
    }
}

export default App;
