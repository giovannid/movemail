# MoveMail app

With this tool you can move your emails from one server to another server. You just need the IMAP settings from both servers to use this tool. 

![Screenshot1](screenshots/screenshot1.png)
![Screenshot1](screenshots/screenshot2.png)
![Screenshot1](screenshots/screenshot3.png)
![Screenshot1](screenshots/screenshot4.png)
